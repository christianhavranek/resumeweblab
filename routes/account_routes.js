var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');
var address_dal = require('../model/address_dal');


// View All accounts
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.getById(req.query.account_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('account/accountViewById', {'result': result});
            }
        });
    }
});

router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually

        account_dal.getTables(function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
              var tables = {
                  'school': result[0],
                  'company': result[1],
                  'skill': result[2]
            };
            res.render('account/accountAdd', tables);
        }
        });
});

router.get('/insert', function(req, res){
    // simple validation
    if(req.query.first_name == null) {
        res.send('A first name must be provided.');
    }
    else if(req.query.last_name == null) {
        res.send('A last name must be provided');
    }
    else if(req.query.email == null) {
        res.send('An email must be provided');
    }
    else if(req.query.school_id == null) {
        res.send('A school id must be provided');
    }
    else if(req.query.company_id == null) {
        res.send('A company id must be provided');
    }
    else if(req.query.skill_id == null) {
        res.send('A skill id must be provided');
    }
    else {
        var accountInfo =
            {
                'first_name': req.query.first_name,
                'last_name': req.query.last_name,
                'email': req.query.email,
                'school_id': req.query.school_id,
                'company_id': req.query.company_id,
                'skill_id': req.query.skill_id
            };
        // passing all the query parameters (req.query) to the insert function instead of each individually
        account_dal.insert(accountInfo, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
            }
        });
    }
});

// Return the add a new company form
/*router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    address_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('company/companyAdd', {'address': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.company_name == null) {
        res.send('Company Name must be provided.');
    }
    else if(req.query.address_id == null) {
        res.send('At least one address must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        company_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/company/all');
            }
        });
    }
});
*/

module.exports = router;
