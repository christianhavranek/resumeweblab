var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(address_id, callback) {
    /*var query = 'SELECT c.*, a.street, a.zip_code FROM account c ' +
        'LEFT JOIN account_address ca on ca.account_id = c.account_id ' +
        'LEFT JOIN address a on a.address_id = ca.address_id ' +
        'WHERE c.account_id = ?';*/
    var query = 'SELECT street, zip_code FROM address ' +
        'WHERE address_id = ?';
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE Address
    var query = 'INSERT INTO address (street, zip_code) VALUES (?,?)';

    var queryData = [params.street, params.zip_code];

    //connection.query(query, queryData, function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        //var address_id = result.insertId;

        //var add_id = 'INSERT INTO address (address_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var address_data = ([params.street, params.zip_code]);

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, address_data, function (err, result) {
            callback(err, result);
        });

   //});
};

exports.edit = function(address_id, callback) {
    var query = 'select * from address WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.update = function(params, callback) {
    var query = 'UPDATE address SET street = ?, zip_code = ? WHERE address_id = ?';
    var queryData = [params.street, params.zip_code, params.address_id];

    connection.query(query, queryData, function(err, result) {
                callback(err, result);
    });
};


exports.delete = function(address_id, callback) {
    var query = 'DELETE FROM address WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};