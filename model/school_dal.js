var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM school;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(school_id, callback) {
    var query = 'SELECT s.*, a.street, a.zip_code FROM school s '+
        'LEFT JOIN school_address sa on sa.school_id = s.school_id ' +
        'LEFT JOIN address a on a.address_id = sa.address_id ' +
        'WHERE s.school_id = ?';
    var queryData = [school_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback)
{

    // FIRST INSERT THE school
    var query = 'INSERT INTO school (school_name) VALUES (?)';

    var queryData = ([params.school_name]);

    connection.query(query, queryData, function(err, result)
    {

        // THEN USE THE school_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO school_ADDRESS
        var school_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO school_address (school_id, address_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var schoolAddressData = [];
        if (params.address_id.constructor === Array) {
            for (var i = 0; i < params.address_id.length; i++) {
                schoolAddressData.push([school_id, params.address_id[i]]);
            }
        }
       else {
                schoolAddressData.push([school_id, params.address_id]);
            }

        // NOTE THE EXTRA [] AROUND schoolAddressData
        connection.query(query, [schoolAddressData], function(err, result){
            callback(err, result);
        });
    });

};


exports.delete = function(school_id, callback) {

    var query2 = 'DELETE FROM school_address WHERE school_id = ?';

    var queryData = ([school_id]);

    connection.query(query2, queryData, function(err, result){

        var query = 'DELETE FROM school WHERE school_id = ?';
        var queryData = [school_id];



        connection.query(query, queryData, function(err, result) {
            callback(err, result);

        });
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE school SET school_name = ? WHERE school_id = ?';
    var queryData = [params.school_name, params.school_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        schoolAddressDeleteAll(params.school_id, function(err, result){

            if(params.address_id != null) {
                //insert company_address ids
                schoolAddressInsert(params.school_id, params.address_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};




//declare the function so it can be used locally
var schoolAddressInsert = function(school_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO school_address (school_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var schoolAddressData = [];
        for (var i = 0; i < addressIdArray.length; i++) {
            schoolAddressData.push([school_id, addressIdArray[i]]);
        }
    connection.query(query, [schoolAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.schoolAddressInsert = schoolAddressInsert;

//declare the function so it can be used locally
var schoolAddressDeleteAll = function(school_id, callback){
    var query = 'DELETE FROM school_address WHERE school_id = ?';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.schoolAddressDeleteAll = schoolAddressDeleteAll;




exports.edit = function(school_id, callback) {
    var query = 'CALL school_getinfo4(?)';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

