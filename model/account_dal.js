var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    /*var query = 'SELECT c.*, a.street, a.zip_code FROM account c ' +
        'LEFT JOIN account_address ca on ca.account_id = c.account_id ' +
        'LEFT JOIN address a on a.address_id = ca.address_id ' +
        'WHERE c.account_id = ?';*/
    var query = 'SELECT first_name, email FROM account ' +
        'WHERE account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.getTables = function(callback) {

    var query = "call get_tables()";
    connection.query(query, function(err, result) {
        callback(err, result)
    });

};

exports.insert = function(params, callback)
{

    // FIRST INSERT THE school
    var query = 'INSERT INTO account (first_name, last_name, email) VALUES (?, ?, ?)';

    var queryData = ([params.first_name, params.last_name, params.email]);


    connection.query(query, queryData, function(err, result)
    {
        if (err){
            callback(err, null);
        }
        else {
        // THEN USE THE school_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO school_ADDRESS
        var account_id = result.insertId;

        var account_company = [];
        for (var i = 0; i < params.company_id.length; i++)
        {
            account_company.push([account_id, params.company_id[i]]);
        }

         var account_school = [];
         for (var i = 0; i < params.school_id.length; i++)
            {
                account_school.push([account_id, params.school_id[i]]);
            }

            var account_skill = [];
            for (var i = 0; i < params.skill_id.length; i++)
            {
                account_skill.push([account_id, params.skill_id[i]]);
            }


        }

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account_company (company_id, account_id) VALUES ?';
        connection.query(query, [account_company], function(err, result){
            if (err)
            {
                callback(err, null);
            }
            else
            {
                var query = 'INSERT INTO account_school (school_id, account_id) VALUES ?';
                connection.query(query, [account_school], function(err, result){
                    if (err)
                    {
                        callback(err, null);
                    }
                    else
                    {
                        var query = 'INSERT INTO account_skill (skill_id, account_id) VALUES ?';
                        connection.query(query, [account_skill], function(err, result) {
                            if (err) {
                                callback(err, null);
                            }
                            else {
                                callback(err, result);
                            }

                        });
                    }
                });
            }
        });
    });
};

